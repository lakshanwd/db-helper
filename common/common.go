package common

import (
	"database/sql"
)

//ParseRow - function for parse data from sql cursor
type ParseRow func(rows *sql.Rows, collection *[]interface{})
