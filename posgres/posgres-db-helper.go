package posgres

import (
	"database/sql"

	"gitlab.com/lakshanwd/db-helper/common"
)

//ExecuteReader - executes @query and returns results in a list
func ExecuteReader(db *sql.DB, query string, fn common.ParseRow, params ...interface{}) (data []interface{}, err error) {
	data = make([]interface{}, 0)
	var rows *sql.Rows
	if rows, err = db.Query(query, params...); err == nil {
		defer rows.Close()
		for rows.Next() {
			fn(rows, &data)
		}
		err = rows.Err()
	}
	return
}

//ExecuteInsert - performs a insert operation and returns latest inserted id
func ExecuteInsert(db *sql.DB, query string, params ...interface{}) (lastInsertedID int64, err error) {
	lastInsertedID = -1
	err = db.QueryRow(query, params...).Scan(lastInsertedID)
	return
}

//ExecuteUpdateDelete - performs update and returns affected row count
func ExecuteUpdateDelete(db *sql.DB, query string, params ...interface{}) (updatedRowCount int64, err error) {
	updatedRowCount = 0
	var stmt *sql.Stmt
	if stmt, err = db.Prepare(query); err == nil {
		defer stmt.Close()
		var result sql.Result
		if result, err = stmt.Exec(params...); err == nil {
			updatedRowCount, err = result.RowsAffected()
		}
	}
	return
}
