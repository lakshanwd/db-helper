### usage

> Initial setup
```go
// import MariaDB/MySQL driver
import "database/sql"
import _ "github.com/go-sql-driver/mysql"

// import db-helper for MariaDB/MySQL
import util "gitlab.com/lakshanwd/db-helper/mysql"

// open up a database connection
db, err := sql.Open("mysql", "username:password@tcp(host:port)/db_name")
if err != nil {
    log.Fatal(err)
}
defer db.Close()

// set connection properties
db.SetMaxIdleConns(10)
db.SetMaxOpenConns(20)

// validate connectivity
if err = db.Ping(); err != nil {
    log.Fatal(err)
}

// definition of Book
type Book struct{
    BookID int
    BookName string
    Author string
}
```

> Read
```go
// delegate function for read query results
reader := func(rows *sql.Rows, collection *[]interface{}) {
    var book Book
    err := rows.Scan(&book.BookID, &book.BookName, &book.Author)
    if err != nil {
        log.Fatal(err)
    }
    *collection = append(*collection, book)
}

// read from database
bookList, err := util.ExecuteReader(db, "select book_id, book_name, book_author from tbl_book", reader)
```

> Update a record
```go
var book Book
...

// update exciting records
updatedRowCount, err := util.ExecuteUpdateDelete(db, "update tbl_book set book_name=?, book_author=? where book_id=?", book.BookName, book.Author, book.BookID)
```

> Delete a record
```go
var book Book
...

// delete from database
deletedRecordCount, err := util.ExecuteUpdateDelete(db, "delete from tbl_book where book_id=?", book.BookID)
```